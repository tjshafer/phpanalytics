<?php

/**
 * Format the page titles.
 */
function formatTitle(string|array $value = null): ?string
{
    if (is_array($value)) {
        return implode(' - ', $value);
    }

    return $value;
}

/**
 * Format money.
 */
function formatMoney(float $amount, string $currency, bool $separator = true, bool $translate = true): string
{
    if (in_array(strtoupper($currency), config('currencies.zero_decimals'))) {
        return number_format($amount, 0, $translate ? __('.') : '.', $separator ? ($translate ? __(',') : ',') : false);
    }

    return number_format($amount, 2, $translate ? __('.') : '.', $separator ? ($translate ? __(',') : ',') : false);
}

/**
 * Get and format the Gravatar URL.
 */
function gravatar(string $email, int $size = 80, string $default = 'identicon', string $rating = 'g'): string
{
    $url = 'https://www.gravatar.com/avatar/';
    $url .= md5(strtolower(trim($email)));
    $url .= '?s='.$size.'&d='.$default.'&r='.$rating;

    return $url;
}

/**
 * Format the browser icon
 */
function formatBrowser(string $key): string
{
    $browsers = [
        'Chrome' => 'chrome',
        'Chromium' => 'chromium',
        'Firefox' => 'firefox',
        'Firefox Mobile' => 'firefox',
        'Edge' => 'edge',
        'Internet Explorer' => 'ie',
        'Mobile Internet Explorer' => 'ie',
        'Vivaldi' => 'vivaldi',
        'Brave' => 'brave',
        'Safari' => 'safari',
        'Opera' => 'opera',
        'Opera Mini' => 'opera',
        'Opera Mobile' => 'opera',
        'Opera Touch' => 'operatouch',
        'Yandex Browser' => 'yandex',
        'UC Browser' => 'ucbrowser',
        'Samsung Internet' => 'samsung',
        'QQ Browser' => 'qq',
        'BlackBerry Browser' => 'bbbrowser',
        'Maxthon' => 'maxthon',
    ];

    if (array_key_exists($key, $browsers)) {
        return $browsers[$key];
    }

    return 'unknown';
}

/**
 * Format the operating system icon
 */
function formatOperatingSystem(string $key): string
{
    $operatingSystems = [
        'Windows' => 'windows',
        'Linux' => 'linux',
        'Ubuntu' => 'ubuntu',
        'Windows Phone' => 'windows',
        'iOS' => 'apple',
        'OS X' => 'apple',
        'FreeBSD' => 'freebsd',
        'Android' => 'android',
        'Chrome OS' => 'chromeos',
        'BlackBerry OS' => 'bbos',
        'Tizen' => 'tizen',
        'KaiOS' => 'kaios',
        'BlackBerry Tablet OS' => 'bbos',
    ];

    if (array_key_exists($key, $operatingSystems)) {
        return $operatingSystems[$key];
    }

    return 'unknown';
}

/**
 * Format the devices icon
 */
function formatDevice(string $key): string
{
    $devices = [
        'desktop' => 'desktop',
        'mobile' => 'mobile',
        'tablet' => 'tablet',
        'television' => 'tv',
        'gaming' => 'gaming',
        'watch' => 'watch',
    ];

    if (array_key_exists($key, $devices)) {
        return $devices[$key];
    }

    return 'unknown';
}

/**
 * Format the flag icon
 */
function formatFlag(string $value): string
{
    $country = explode(':', $value);

    if (isset($country[0]) && ! empty($country[0])) {
        // Return the country code
        return strtolower($country[0]);
    }

    return 'unknown';
}

/**
 * Convert a number into a readable one.
 */
function shortenNumber(int $number): string|int
{
    $suffix = ['', 'K', 'M', 'B'];
    $precision = 1;
    for ($i = 0; $i < count($suffix); $i++) {
        $divide = $number / pow(1000, $i);
        if ($divide < 1000) {
            return round($divide, $precision).$suffix[$i];
        }
    }

    return $number;
}
