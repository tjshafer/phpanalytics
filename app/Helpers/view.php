<?php

/**
 * Calculate the growth between two values.
 */
function calcGrowth(mixed $current, mixed $previous): int
{
    if ($previous == 0 || $previous == null || $current == 0) {
        return 0;
    }

    return ($current - $previous) / $previous * 100;
}
