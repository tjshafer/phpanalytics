<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\API\StoreWebsiteRequest;
use App\Http\Requests\API\UpdateWebsiteRequest;
use App\Http\Resources\WebsiteResource;
use App\Models\Website;
use App\Traits\WebsiteTrait;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class WebsiteController extends Controller
{
    use WebsiteTrait;

    /**
     * Display a listing of the resource.
     */
    public function index(Request $request): AnonymousResourceCollection
    {
        $search = $request->input('search');
        $searchBy = in_array($request->input('search_by'), ['domain']) ? $request->input('search_by') : 'domain';
        $sortBy = in_array($request->input('sort_by'), ['id', 'domain']) ? $request->input('sort_by') : 'id';
        $sort = in_array($request->input('sort'), ['asc', 'desc']) ? $request->input('sort') : 'desc';
        $perPage = in_array($request->input('per_page'), [10, 25, 50, 100]) ? $request->input('per_page') : config('settings.paginate');

        return WebsiteResource::collection(Website::query()->where('user_id', $request->user()->id)
            ->when($search, function ($query) use ($search) {
                return $query->searchDomain($search);
            })
            ->orderBy($sortBy, $sort)
            ->paginate($perPage)
            ->appends(['search' => $search, 'search_by' => $searchBy, 'sort_by' => $sortBy, 'sort' => $sort, 'per_page' => $perPage]))
            ->additional(['status' => 200]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreWebsiteRequest $request): WebsiteResource|JsonResponse
    {
        $created = $this->websiteStore($request);

        if ($created) {
            return WebsiteResource::make($created);
        }

        return response()->json([
            'message' => __('Resource not found.'),
            'status' => 404,
        ], 404);
    }

    /**
     * Display the specified resource.
     */
    public function show(Request $request, int $id): WebsiteResource|JsonResponse
    {
        $link = Website::query()->where([['id', '=', $id], ['user_id', $request->user()->id]])->first();

        if ($link) {
            return WebsiteResource::make($link);
        }

        return response()->json([
            'message' => __('Resource not found.'),
            'status' => 404,
        ], 404);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateWebsiteRequest $request, int $id): WebsiteResource|JsonResponse
    {
        $website = Website::query()->where([['id', '=', $id], ['user_id', '=', $request->user()->id]])->firstOrFail();

        $updated = $this->websiteUpdate($request, $website);

        if ($updated) {
            return WebsiteResource::make($updated);
        }

        return response()->json([
            'message' => __('Resource not found.'),
        ], 404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @throws \Exception
     */
    public function destroy(Request $request, int $id): JsonResponse
    {
        $website = Website::query()->where([['id', '=', $id], ['user_id', '=', $request->user()->id]])->first();

        if ($website) {
            $website->delete();

            return response()->json([
                'id' => $website->id,
                'object' => 'website',
                'deleted' => true,
                'status' => 200,
            ]);
        }

        return response()->json([
            'message' => __('Resource not found.'),
        ], 404);
    }
}
