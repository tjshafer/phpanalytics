<?php

namespace App\Http\Controllers;

use App\Http\Requests\ContactMailRequest;
use App\Mail\ContactMail;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Mail;

class ContactController extends Controller
{
    /**
     * Show the Contact page.
     */
    public function index(): View
    {
        return view('contact.index');
    }

    /**
     * Send the Contact email.
     */
    public function send(ContactMailRequest $request): RedirectResponse
    {
        try {
            Mail::to(config('settings.contact_email'))->send(new ContactMail());
        } catch(\Exception $e) {
            return to_route('contact')->with('error', $e->getMessage());
        }

        return to_route('contact')->with('success', __('Thank you!').' '.__('We\'ve received your message.'));
    }
}
