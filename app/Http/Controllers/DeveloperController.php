<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\View\View;

class DeveloperController extends Controller
{
    /**
     * Show the Developer index page.
     */
    public function index(): View
    {
        return view('developers.index');
    }

    /**
     * Show the Developer Stats page.
     */
    public function stats(): View
    {
        return view('developers.stats.index');
    }

    /**
     * Show the Developer Websites page.
     */
    public function websites(): View
    {
        return view('developers.websites.index');
    }

    /**
     * Show the Developer Account page.
     */
    public function account(): View
    {
        return view('developers.account.index');
    }
}
