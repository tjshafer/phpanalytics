<?php

namespace App\Http\Controllers;

use App\Models\Page;
use Illuminate\Contracts\View\View;

class PageController extends Controller
{
    /**
     * Show the page.
     */
    public function show(string $id): View
    {
        $page = Page::query()->where('slug', $id)->firstOrFail();

        return view('pages.show', ['page' => $page]);
    }
}
