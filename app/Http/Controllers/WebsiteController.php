<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreWebsiteRequest;
use App\Http\Requests\UpdateWebsiteRequest;
use App\Models\Website;
use App\Traits\DateRangeTrait;
use App\Traits\WebsiteTrait;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class WebsiteController extends Controller
{
    use WebsiteTrait, DateRangeTrait;

    /**
     * Show the create Website form.
     */
    public function create(): View
    {
        return view('websites.container', ['view' => 'new']);
    }

    /**
     * Show the edit Website form.
     */
    public function edit(Request $request, int $id): View
    {
        $website = Website::query()->where([['id', '=', $id], ['user_id', '=', $request->user()->id]])->firstOrFail();

        return view('websites.container', ['view' => 'edit', 'website' => $website]);
    }

    /**
     * Store the Website.
     */
    public function store(StoreWebsiteRequest $request): RedirectResponse
    {
        $this->websiteStore($request);

        $request->user()->has_websites = true;
        $request->user()->save();

        return to_route('dashboard')->with('success', __(':name has been created.', ['name' => $request->input('domain')]));
    }

    /**
     * Update the Website.
     */
    public function update(UpdateWebsiteRequest $request, int $id): RedirectResponse
    {
        $website = Website::query()->where([['id', '=', $id], ['user_id', '=', $request->user()->id]])->firstOrFail();

        $this->websiteUpdate($request, $website);

        return redirect()->back()->with('success', __('Settings saved.'));
    }

    /**
     * Delete the Website.
     *
     * @throws \Exception
     */
    public function destroy(Request $request, int $id): RedirectResponse
    {
        $website = Website::query()->where([['id', '=', $id], ['user_id', '=', $request->user()->id]])->firstOrFail();

        $website->delete();

        $request->user()->has_websites = Website::query()->where('user_id', '=', $request->user()->id)->count() > 0;
        $request->user()->save();

        return to_route('dashboard')->with('success', __(':name has been deleted.', ['name' => $website->domain]));
    }
}
