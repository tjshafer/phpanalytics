<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class VerifyPaymentEnabledMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @return mixed
     */
    public function handle(Request $request, Closure $next): mixed
    {
        // Check if any payment processor is enabled
        if (! paymentProcessors()) {
            return to_route('home');
        }

        return $next($request);
    }
}
