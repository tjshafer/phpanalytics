<?php

namespace App\Http\Requests\API;

use App\Http\Requests\StoreWebsiteRequest as BaseStoreWebsiteRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class StoreWebsiteRequest extends BaseStoreWebsiteRequest
{
    /**
     * Handle a failed validation attempt.
     */
    protected function failedValidation(Validator $validator): void
    {
        throw new HttpResponseException(
            response()->json([
                'message' => $validator->errors(),
                'status' => 422,
            ], 422));
    }
}
