<?php

namespace App\Http\Requests\API;

use App\Http\Requests\UpdateWebsiteRequest as BaseUpdateWebsiteRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class UpdateWebsiteRequest extends BaseUpdateWebsiteRequest
{
    /**
     * Handle a failed validation attempt.
     */
    protected function failedValidation(Validator $validator): void
    {
        throw new HttpResponseException(
            response()->json([
                'message' => $validator->errors(),
                'status' => 422,
            ], 422));
    }
}
