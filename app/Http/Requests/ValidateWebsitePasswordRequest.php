<?php

namespace App\Http\Requests;

use App\Models\Website;
use App\Rules\ValidateWebsitePasswordRule;
use Illuminate\Foundation\Http\FormRequest;

class ValidateWebsitePasswordRequest extends FormRequest
{
    /**
     * @var
     */
    public Website $website;

    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        $this->website = Website::query()->where('domain', $this->route('id'))->firstOrFail();

        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'password' => ['required', new ValidateWebsitePasswordRule($this->website)],
        ];
    }
}
