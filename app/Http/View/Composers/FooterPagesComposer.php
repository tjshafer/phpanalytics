<?php

namespace App\Http\View\Composers;

use App\Models\Page;
use Illuminate\Contracts\View\View;

class FooterPagesComposer
{
    /**
     * Bind data to the view.
     */
    public function compose(View $view): void
    {
        try {
            $footerPages = Page::query()->where('visibility', '<>', 0)->get();
        } catch (\Exception $e) {
            $footerPages = [];
        }

        $view->with('footerPages', $footerPages);
    }
}
