<?php

namespace App\Http\View\Composers;

use App\Models\Website;
use App\Traits\DateRangeTrait;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Auth;

class UserWebsitesComposer
{
    use DateRangeTrait;

    /**
     * Bind data to the view.
     */
    public function compose(View $view): void
    {
        if (Auth::check()) {
            $user = Auth::user();

            $websites = Website::query()->where('user_id', $user->id)->oldest('domain')->get();

            $view->with('websites', $websites)->with('range', $this->range());
        }
    }
}
