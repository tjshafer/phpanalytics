<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ContactMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     */
    public function __construct()
    {
        //
    }

    /**
     * Build the message.
     */
    public function build(): static
    {
        return $this->from(config('settings.email_address'))
            ->replyTo(request()->input('email'))
            ->subject(formatTitle([request()->input('subject'), config('settings.title')]))
            ->markdown('emails.contact', ['message' => request()->input('message')]);
    }
}
