<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ReportMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     */
    public function __construct(public array $stats, public array $range)
    {
    }

    /**
     * Build the message.
     */
    public function build(): static
    {
        $this->subject(formatTitle([__('Periodic report'), config('settings.title')]));

        return $this->markdown('emails.report', [
            'introLines' => [__('Your periodic report is ready.')],
        ]);
    }
}
