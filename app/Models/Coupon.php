<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Coupon extends Model
{
    use HasFactory;
    use SoftDeletes;

    /**
     * Scope a query to only include coupons with a given name.
     */
    public function scopeSearchName(Builder $query, string $value): Builder
    {
        return $query->where('name', 'like', '%'.$value.'%');
    }

    /**
     * Scope a query to only include coupons with a given code.
     */
    public function scopeSearchCode(Builder $query, string $value): Builder
    {
        return $query->where('code', 'like', '%'.$value.'%');
    }

    /**
     * Scope a query to only include coupons with a given type.
     */
    public function scopeOfType(Builder $query, string $value): Builder
    {
        return $query->where('type', '=', $value);
    }
}
