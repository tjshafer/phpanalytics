<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    use HasFactory;

    /**
     * {@inheritdoc}
     */
    public $fillable = [
        'name', 'slug', 'footer', 'content',
    ];

    /**
     * Scope a query to only include pages with a given name.
     */
    public function scopeSearchName(Builder $query, string $value): Builder
    {
        return $query->where('name', 'like', '%'.$value.'%')->orWhere('content', 'like', '%'.$value.'%');
    }
}
