<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Payment extends Model
{
    use HasFactory;

    /**
     * {@inheritdoc}
     */
    protected $casts = [
        'product' => 'object',
        'tax_rates' => 'object',
        'coupon' => 'object',
        'customer' => 'object',
        'seller' => 'object',
    ];

    /**
     * Get the user that owns the payment.
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class)->withTrashed();
    }

    /**
     * Get the plan of the payment.
     */
    public function plan(): BelongsTo
    {
        return $this->belongsTo(Plan::class)->withTrashed();
    }

    /**
     * Scope a query to only include payments with a given payment id.
     */
    public function scopeSearchPayment(Builder $query, string $value): Builder
    {
        return $query->where('payment_id', 'like', '%'.$value.'%');
    }

    /**
     * Scope a query to only include payments with a given invoice id.
     */
    public function scopeSearchInvoice(Builder $query, string $value): Builder
    {
        return $query->where([['invoice_id', 'like', '%'.$value.'%'], ['status', '<>', 'pending']]);
    }

    /**
     *Scope a query to only include payments that are not pending.
     */
    public function scopeOfPlan(Builder $query, string $value): Builder
    {
        return $query->where('plan_id', '=', $value);
    }

    /**
     * Scope a query to only include payments with a given status.
     */
    public function scopeOfUser(Builder $query, string $value): Builder
    {
        return $query->where('user_id', '=', $value);
    }

    /**
     * Scope a query to only include payments with a given status.
     */
    public function scopeOfInterval(Builder $query, string $value): Builder
    {
        return $query->where('interval', '=', $value);
    }

    /**
     * Scope a query to only include payments with a given status.
     */
    public function scopeOfProcessor(Builder $query, string $value): Builder
    {
        return $query->where('processor', '=', $value);
    }

    /**
     * Scope a query to only include payments with a given status.
     */
    public function scopeOfStatus(Builder $query, string $value): Builder
    {
        return $query->where('status', '=', $value);
    }
}
