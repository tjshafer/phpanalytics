<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Plan extends Model
{
    use HasFactory;
    use SoftDeletes;

    /**
     * {@inheritdoc}
     */
    protected $casts = [
        'items' => 'object',
        'tax_rates' => 'object',
        'coupons' => 'object',
        'features' => 'object',
    ];

    /**
     * Scope a query to only include plans with a given name.
     */
    public function scopeSearchName(Builder $query, string $value): Builder
    {
        return $query->where('name', 'like', '%'.$value.'%');
    }

    /**
     * Scope a query to only include plans with a given visibility.
     */
    public function scopeOfVisibility(Builder $query, string $value): Builder
    {
        return $query->where('visibility', '=', $value);
    }

    /**
     *Scope a query to only include plans that are not the default plan.
     */
    public function scopeNotDefault(Builder $query): Builder
    {
        return $query->where('id', '>', 1);
    }

    /**
     * Check if the Plan is the default plan.
     */
    public function isDefault(): bool
    {
        return $this->id == 1;
    }
}
