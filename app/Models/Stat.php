<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Stat extends Model
{
    use HasFactory;

    /**
     * {@inheritdoc}
     */
    protected $casts = [
        'date' => 'datetime:Y-m-d',
    ];

    /**
     * Scope a query to only include stats for a given website.
     */
    public function scopeSearchValue(Builder $query, string $value): Builder
    {
        return $query->where('value', 'like', '%'.$value.'%');
    }
}
