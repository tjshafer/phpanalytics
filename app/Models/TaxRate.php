<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TaxRate extends Model
{
    use HasFactory;
    use SoftDeletes;

    /**
     * {@inheritdoc}
     */
    protected $casts = [
        'regions' => 'object',
    ];

    /**
     * Scope a query to only include tax rates with a given name.
     */
    public function scopeSearchName(Builder $query, string $value): Builder
    {
        return $query->where('name', 'like', '%'.$value.'%');
    }

    /**
     * Scope a query to only include tax rates with a given type.
     */
    public function scopeOfType(Builder $query, string $value): Builder
    {
        return $query->where('type', '=', $value);
    }

    /**
     * Scope a query to only include tax rates with a given region.
     */
    public function scopeOfRegion(Builder $query, string $value): Builder
    {
        $query->whereNull('regions')
            ->when($value, function ($query) use ($value) {
                $query->orWhere('regions', 'like', '%'.$value.'%');
            });

        return $query;
    }
}
