<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Facades\Crypt;

class Website extends Model
{
    use HasFactory;

    /**
     * {@inheritDoc}
     */
    protected $fillable = [
        'name',
        'domain',
        'status',
        'user_id',
        'created_at',
        'updated_at',
    ];

    /**
     * Scope a query to only include websites with a given name.
     */
    public function scopeSearchDomain(Builder $query, string $value): Builder
    {
        return $query->where('domain', 'like', '%'.$value.'%');
    }

    /**
     * Get the user that owns the website.
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(\App\Models\User::class)->withTrashed();
    }

    /**
     * Scope a query to only include websites of a given user.
     */
    public function scopeOfUser(Builder $query, string $value): Builder
    {
        return $query->where('user_id', '=', $value);
    }

    /**
     * Get the visitors count for a specific date range.
     */
    public function visitors(): HasMany
    {
        return $this->hasMany(Stat::class, 'website_id', 'id')
            ->where('name', '=', 'visitors');
    }

    /**
     * Get the pageviews count for a specific date range.
     */
    public function pageviews(): HasMany
    {
        return $this->hasMany(Stat::class, 'website_id', 'id')
            ->where('name', '=', 'pageviews');
    }

    /**
     * Get the website's stats.
     */
    public function stats(): HasMany
    {
        return $this->hasMany(Stat::class)->where('website_id', $this->id);
    }

    /**
     * Get the website's recent stats.
     */
    public function recents(): HasMany
    {
        return $this->hasMany(Recent::class)->where('website_id', $this->id);
    }

    /**
     * Password attribute.
     */
    protected function password(): Attribute
    {
        return new Attribute(
            get: function ($value) {
                try {
                    return Crypt::decryptString($value);
                } catch (\Exception $e) {
                    return null;
                }
            },
            set: fn ($value) => Crypt::encryptString($value),
        );
    }
}
