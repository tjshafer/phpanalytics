<?php

namespace App\Observers;

use App\Models\Website;

class WebsiteObserver
{
    /**
     * Handle the Website "deleting" event.
     */
    public function deleting(Website $website): void
    {
        $website->stats()->delete();
        $website->recents()->delete();
    }
}
