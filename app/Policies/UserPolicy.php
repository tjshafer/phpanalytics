<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;

class UserPolicy
{
    use HandlesAuthorization;

    public function before(User $user): Response
    {
        if (! $user->plan) {
            return $this->allow();
        }

        return $this->deny();
    }

    /**
     * Determine whether the user can use Data Export.
     */
    public function dataExport(User $user): bool
    {
        return $user->plan->features->data_export;
    }

    /**
     * Determine whether the user can use the API.
     */
    public function api(User $user): bool
    {
        return $user->plan->features->api;
    }

    /**
     * Determine whether the user can receive Email Reports.
     */
    public function emailReports(User $user): bool
    {
        return $user->plan->features->email_reports;
    }
}
