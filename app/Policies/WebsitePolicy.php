<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Website;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;

class WebsitePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can create models.
     */
    public function create(User $user): Response
    {
        if ($user->plan->features->websites == -1) {
            return $this->allow();
        }
        if ($user->plan->features->websites > 0) {
            $count = Website::query()->where('user_id', '=', $user->id)->count();
            if ($count < $user->plan->features->websites) {
                return $this->allow();
            }
        }

        return $this->deny();
    }
}
