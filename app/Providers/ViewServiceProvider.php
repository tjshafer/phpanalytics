<?php

namespace App\Providers;

use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ViewServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     */
    public function register(): void
    {
        //
    }

    /**
     * Bootstrap services.
     */
    public function boot(): void
    {
        View::composer('shared.footer', \App\Http\View\Composers\FooterPagesComposer::class);

        View::composer([
            'shared.header',
            'account.plan',
        ], \App\Http\View\Composers\UserStatsComposer::class);

        View::composer([
            'shared.sidebars.user',
        ], \App\Http\View\Composers\UserWebsitesComposer::class);
    }
}
