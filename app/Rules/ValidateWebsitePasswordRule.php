<?php

namespace App\Rules;

use App\Models\Website;
use Illuminate\Contracts\Validation\Rule;

class ValidateWebsitePasswordRule implements Rule
{
    public function __construct(private Website $website)
    {
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     */
    public function passes($attribute, $value): bool
    {
        if ($this->website->password == $value) {
            return true;
        }

        return false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('The entered password is not correct.');
    }
}
