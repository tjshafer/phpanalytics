<?php

namespace App\Rules;

use App\Models\User;
use App\Models\Website;
use Illuminate\Contracts\Validation\Rule;

class WebsiteLimitGateRule implements Rule
{
    public function __construct(private User $user)
    {
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if ($this->user->can('create', [Website::class])) {
            return true;
        }

        return false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('You created too many websites.');
    }
}
