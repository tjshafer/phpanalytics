<?php

namespace App\Traits;

use App\Models\Website;
use Illuminate\Http\Request;

trait WebsiteTrait
{
    /**
     * Store the Website.
     */
    protected function websiteStore(Request $request): Website
    {
        $website = $request->user()->websites()->create([
            'domain' => $request->input('domain'),
            'privacy' => $request->input('privacy'),
            'password' => $request->input('password'),
            'email' => $request->input('email'),
            'exclude_bots' => ($request->has('exclude_bots') ? $request->input('exclude_bots') : 1),
            'exclude_params' => $request->input('exclude_params'),
            'exclude_ips' => $request->input('exclude_ips'),
        ]);

        return $website;
    }

    /**
     * Update the Website.
     */
    protected function websiteUpdate(Request $request, Website $website): Website
    {
        if ($request->has('privacy')) {
            $website->privacy = $request->input('privacy');
        }

        if ($request->has('email')) {
            $website->email = $request->input('email');
        }

        if ($request->has('password')) {
            $website->password = $request->input('password');
        }

        if ($request->has('exclude_bots')) {
            $website->exclude_bots = $request->input('exclude_bots');
        }

        if ($request->has('exclude_params')) {
            $website->exclude_params = $request->input('exclude_params');
        }

        if ($request->has('exclude_ips')) {
            $website->exclude_ips = $request->input('exclude_ips');
        }

        $website->save();

        return $website;
    }
}
