<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Website>
 */
class WebsiteFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'domain' => $this->faker->url,
            'user_id' => \App\Models\User::factory(),
            'privacy' => $this->faker->boolean,
            'password' => $this->faker->password,
            'email' => $this->faker->email,
            'exclude_bots' => $this->faker->boolean,
            'exclude_ips' => $this->faker->boolean,
        ];
    }
}
