@guest
    <div id="header" class="shadow header sticky-top bg-base-0 z-1025">
        <div class="container">
            <nav class="px-0 py-3 navbar navbar-expand-lg navbar-light">
                <a href="{{ route('home') }}" aria-label="{{ config('settings.title') }}" class="p-0 navbar-brand">
                    <div class="logo">
                        <img src="{{ url('/') }}/uploads/brand/{{ (config('settings.dark_mode') == 1 ? config('settings.logo_dark') : config('settings.logo')) }}" alt="{{ config('settings.title') }}" data-theme-dark="{{ url('/') }}/uploads/brand/{{ config('settings.logo_dark') }}" data-theme-light="{{ url('/') }}/uploads/brand/{{ config('settings.logo') }}" data-theme-target="src">
                    </div>
                </a>
                <button class="p-0 border-0 navbar-toggler" type="button" data-toggle="collapse" data-target="#header-navbar" aria-controls="header-navbar" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="header-navbar">
                    <ul class="navbar-nav pt-2 p-lg-0 {{ (__('lang_dir') == 'rtl' ? 'mr-auto' : 'ml-auto') }}">
                        @if(paymentProcessors())
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('pricing') }}" role="button">{{ __('Pricing') }}</a>
                            </li>
                        @endif

                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('login') }}" role="button">{{ __('Login') }}</a>
                        </li>

                        @if(config('settings.registration'))
                            <li class="nav-item d-flex align-items-center">
                                <a class="btn btn-outline-primary" href="{{ route('register') }}" role="button">{{ __('Register') }}</a>
                            </li>
                        @endif
                    </ul>
                </div>
            </nav>
        </div>
    </div>
@else
    <div id="header" class="shadow header sticky-top bg-base-0 z-1025 d-lg-none">
        <div class="container-fluid">
            <nav class="px-0 py-3 navbar navbar-light">
                <a href="{{ route('dashboard') }}" aria-label="{{ config('settings.title') }}" class="p-0 navbar-brand">
                    <div class="logo">
                        <img src="{{ url('/') }}/uploads/brand/{{ (config('settings.dark_mode') == 1 ? config('settings.logo_dark') : config('settings.logo')) }}" alt="{{ config('settings.title') }}" data-theme-dark="{{ url('/') }}/uploads/brand/{{ config('settings.logo_dark') }}" data-theme-light="{{ url('/') }}/uploads/brand/{{ config('settings.logo') }}" data-theme-target="src">
                    </div>
                </a>
                <button class="p-0 border-0 slide-menu-toggle navbar-toggler" type="button">
                    <span class="navbar-toggler-icon"></span>
                </button>
            </nav>
        </div>
    </div>

    <nav class="p-0 shadow slide-menu bg-base-0 navbar navbar-light d-flex flex-column z-1030" id="slide-menu">
        <div class="sidebar-section flex-grow-1 d-flex flex-column w-100">
            <div>
                <div class="{{ (__('lang_dir') == 'rtl' ? 'pr-4' : 'pl-4') }} py-3 d-flex align-items-center">
                    <a href="{{ route('dashboard') }}" aria-label="{{ config('settings.title') }}" class="p-0 navbar-brand">
                        <div class="logo">
                            <img src="{{ url('/') }}/uploads/brand/{{ (config('settings.dark_mode') == 1 ? config('settings.logo_dark') : config('settings.logo')) }}" alt="{{ config('settings.title') }}" data-theme-dark="{{ url('/') }}/uploads/brand/{{ config('settings.logo_dark') }}" data-theme-light="{{ url('/') }}/uploads/brand/{{ config('settings.logo') }}" data-theme-target="src">
                        </div>
                    </a>
                    <div class="close slide-menu-toggle cursor-pointer d-lg-none d-flex align-items-center {{ (__('lang_dir') == 'rtl' ? 'mr-auto' : 'ml-auto') }} px-4 py-2">
                        @include('icons.close', ['class' => 'fill-current width-4 height-4'])
                    </div>
                </div>
            </div>

            <div class="d-flex align-items-center">
                <div class="py-3 {{ (__('lang_dir') == 'rtl' ? 'pr-4 pl-0' : 'pl-4 pr-0') }} font-weight-medium text-muted text-uppercase flex-grow-1">{{ __('Menu') }}</div>

                @if(Auth::user()->role == 1)
                    @if (request()->is('admin/*'))
                        <a class="px-4 py-2 text-decoration-none text-secondary" href="{{ route('dashboard') }}" data-tooltip="true" title="{{ __('User') }}" role="button"><span class="d-flex align-items-center">@include('icons.account-circle', ['class' => 'width-4 height-4 fill-current'])</span></a>
                    @else
                        <a class="px-4 py-2 text-decoration-none text-secondary" href="{{ route('admin.dashboard') }}" data-tooltip="true" title="{{ __('Admin') }}" role="button"><span class="d-flex align-items-center">@include('icons.supervised-user-circle', ['class' => 'width-4 height-4 fill-current'])</span></a>
                    @endif
                @endif
            </div>

            <div class="overflow-auto sidebar-section flex-grow-1 sidebar">
                @yield('menu')
            </div>

            @if(Auth::user()->plan)
                @if(Auth::user()->plan->features->pageviews >= 0)
                    @if($pageviewsCount >= Auth::user()->plan->features->pageviews)
                        <div class="px-4 pt-3">
                            @if(Auth::user()->can_track)
                                <div class="mb-0 alert alert-warning" role="alert">
                                    <div class="d-flex flex-column">
                                        <div class="d-flex align-items-center small">
                                            {{ __('Your account will be limited.') }} {{ __('Upgrade your account to continue tracking your visitors.') }}
                                        </div>

                                        <div class="mt-3">
                                            <a href="{{ route('pricing') }}" class="btn btn-sm btn-block btn-warning">{{ __('Upgrade') }}</a>
                                        </div>
                                    </div>
                                </div>
                            @else
                                <div class="mb-0 alert alert-danger" role="alert">
                                    <div class="d-flex flex-column">
                                        <div class="d-flex align-items-center small">
                                            {{ __('Your account has been limited.') }} {{ __('Upgrade your account to continue tracking your visitors.') }}
                                        </div>

                                        <div class="mt-3">
                                            <a href="{{ route('pricing') }}" class="btn btn-sm btn-block btn-danger">{{ __('Upgrade') }}</a>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        </div>
                    @endif
                @endif


                <a href="{{ route('account.plan') }}" class="px-2 py-2 mx-3 my-2 text-decoration-none">
                    <div class="row no-gutters">
                        <div class="col">
                            <div class="small text-muted">
                                {{ __(':number of :total pageviews used.', ['number' => shortenNumber($pageviewsCount), 'total' => (Auth::user()->plan->features->pageviews < 0 ? '∞' : shortenNumber(Auth::user()->plan->features->pageviews))]) }}
                            </div>
                        </div>
                    </div>

                    <div class="progress w-100 my-2 height-1.25">
                        <div class="rounded progress-bar bg-danger" role="progressbar" style="width: {{ (Auth::user()->plan->features->pageviews == 0 ? 100 : (($pageviewsCount / Auth::user()->plan->features->pageviews) * 100)) }}%"></div>
                    </div>
                </a>
            @endif

            <div class="sidebar sidebar-footer">
                <div class="py-3 {{ (__('lang_dir') == 'rtl' ? 'pr-4 pl-0' : 'pl-4 pr-0') }} d-flex align-items-center" aria-expanded="true">
                    <a href="{{ route('account') }}" class="overflow-hidden d-flex align-items-center text-secondary text-decoration-none flex-grow-1">
                        <img src="{{ gravatar(Auth::user()->email, 80) }}" class="flex-shrink-0 rounded-circle width-10 height-10 {{ (__('lang_dir') == 'rtl' ? 'ml-3' : 'mr-3') }}">

                        <div class="d-flex flex-column text-truncate">
                            <div class="font-weight-medium text-dark text-truncate">
                                {{ Auth::user()->name }}
                            </div>

                            <div class="small font-weight-medium">
                                {{ __('Account') }}
                            </div>
                        </div>
                    </a>

                    <a class="flex-shrink-0 px-4 py-2 d-flex align-items-center text-secondary" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" data-tooltip="true" title="{{ __('Logout') }}">@include('icons.exit-to-app', ['class' => 'fill-current width-4 height-4'])</a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                        @csrf
                    </form>
                </div>
            </div>
        </div>
    </nav>
@endguest
