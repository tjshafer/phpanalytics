<?php

use App\Models\Recent;

test('the clear recents command clears the recents table', function () {
    // Arrange
    Recent::factory(10)->create();
    $this->artisan('cron:clear-recents');

    $this->assertDatabaseCount('recents', 0);
})->group('commands');
